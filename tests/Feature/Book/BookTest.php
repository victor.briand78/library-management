<?php

namespace Tests\Feature\Book;

use App\Enum\UserAbilitiesEnum;
use App\Models\Book;
use Database\Seeders\UserAbilitySeeder;
use Inertia\Testing\AssertableInertia;
use Inertia\Testing\AssertableInertia as Assert;

it('should render book list page', function () {
    $this->createUserAndLogin();

    $response = $this->get(route('books.index'));

    $response->assertStatus(200)
        ->assertInertia(
            fn (AssertableInertia $page) => $page
                ->component('Book/Index')
        );
});

it('should render detailed book page', function () {
    $this->createUserAndLogin();

    $book = Book::factory()->create();

    $response = $this->get(route('books.show', [
        'book' => $book->id
    ]));

    $response->assertStatus(200)
        ->assertInertia(
            fn (AssertableInertia $page) => $page
                ->component('Book/Show')
                ->where('book.id', $book->id)
        );
});

it('should render create book page', function () {
    $this->createUserAndLogin();

    $response = $this->get(route('books.create'));

    $response->assertStatus(200)
        ->assertInertia(
            fn (AssertableInertia $page) => $page
                ->component('Book/Create')
        );
});

it('should store a new book model', function () {

    $user = $this->createUserAndLogin();

    $this->seed(UserAbilitySeeder::class);

    $user->can(UserAbilitiesEnum::CreateBook->value);

    $response = $this->postJson(route('books.store'), [
        'name' => $name = fake()->sentence(3),
        'author' => $author = fake()->name(),
        'description' => $description = fake()->sentence(18),
    ]);

    $response->assertStatus(200);

    $this->assertDatabaseHas(Book::class, [
        'name' => $name,
        'author' => $author,
        'description' => $description,
    ]);
});

it('should soft delete a book model', function () {

    $user = $this->createUserAndLogin();

    $this->seed(UserAbilitySeeder::class);

    $user->can(UserAbilitiesEnum::DeleteBook->value);

    $book = Book::factory()->create();

    $response = $this->delete(route('books.destroy', [
        'book' => $book->id
    ]));

    $response->assertStatus(302);

    $this->assertSoftDeleted(Book::class, [
        'id' => $book->id,
    ]);
});

it('should update a book model', function () {

    $user = $this->createUserAndLogin();

    $this->seed(UserAbilitySeeder::class);

    $user->can(UserAbilitiesEnum::UpdateBook->value);

    $book = Book::factory()->create();

    $response = $this->postJson(
        route('books.update', [
            'book' => $book->id,
        ]),
        [
            'name' => $name = fake()->sentence(3),
            'author' => $author = fake()->name(),
            'description' => $description = fake()->sentence(10),
            '_method' => 'PUT'
        ]
    );

    $response->assertStatus(200);

    $this->assertDatabaseHas(Book::class, [
        'id' => $book->id,
        'name' => $name,
        'author' => $author,
        'description' => $description,
    ]);
});
