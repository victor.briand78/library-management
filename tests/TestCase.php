<?php

namespace Tests;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication,
        RefreshDatabase;

    protected function createUserAndLogin()
    {
        $this->actingAs($user = User::factory()->create());

        return $user;
    }

    protected function setUp(): void
    {
        parent::setUp();

        $this->withoutVite();
    }
}
