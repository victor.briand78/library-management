<?php

namespace App\Enum;

enum UserAbilitiesEnum: string
{
    case ViewBook = 'view_book';
    case CreateBook = 'create_book';
    case UpdateBook = 'update_book';
    case DeleteBook = 'delete_book';
    case RestoreBook = 'restore_book';
}
