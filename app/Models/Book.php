<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Builder;

/**
 * Book
 *
 * @mixin Builder
 */
class Book extends Model {
    use HasFactory,
        SoftDeletes;

    protected $fillable = [
        'name',
        'author_id',
        'description'
    ];

    protected $with = ['author:name'];

    public function author(): BelongsTo
    {
        return $this->belongsTo(Author::class);
    }
}
