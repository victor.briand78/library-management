<?php

namespace App\Http\Controllers;

use App\Http\Resources\AuthorIndexCollection;
use App\Models\Author;
use App\Http\Requests\StoreAuthorRequest;
use App\Http\Requests\UpdateAuthorRequest;
use App\Models\Book;
use Illuminate\Http\Request;

class AuthorController extends Controller {
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $filters = $request->only('searchItems');
        $query = isset($filters['searchItems'])
            ? new AuthorIndexCollection(
                Author::where('name', 'like', '%' . $filters['searchItems'] . '%')
                    ->select(['id', 'name', 'biography'])
                    ->paginate(10)
                    ->withQueryString())
            : new AuthorIndexCollection(
                Author::select(['id', 'name', 'biography'])
                    ->paginate(10)
                    ->withQueryString());

        return inertia(
            'Author/Index',
            [
                'authors' => $query,
                'filters' => $request->only('searchBooks')
            ]
        );
    }

    /**
     * Display the specified resource.
     */
    public function show(Author $author)
    {
        return inertia('Author/Show', [
            'author' => $author,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return inertia('Author/Create', [
            'labels' => __('authors.form.labels')
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreAuthorRequest $request)
    {
        Author::create($request->validated());

        return inertia('Author/Index', [
            'authors' => new AuthorIndexCollection(Author::all())
        ])
            ->with('success', __('authors.flash.created'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Author $author)
    {
        return inertia(
            'Author/Edit',
            [
                'author' => $author,
                'labels' => __('authors.form.labels')
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateAuthorRequest $request, Author $author)
    {
        $author->update($request->validated());

        return inertia('Author/Show', [
            'author' => $author,
        ])
            ->with('success', __('authors.flash.updated'));
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Author $author)
    {
        $author->delete();

        return redirect()->back()
            ->with('success', __('authors.flash.deleted'));
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroyMany(Request $request)
    {
        $ids = $request->input('ids');

        Author::destroy($ids);

        return redirect()->back()
            ->with('success', __('authors.flash.delete-many'));
    }
}

