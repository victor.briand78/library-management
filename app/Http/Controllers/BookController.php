<?php

namespace App\Http\Controllers;

use App\Models\Author;
use App\Models\Book;
use App\Http\Requests\StoreBookRequest;
use App\Http\Requests\UpdateBookRequest;
use App\Http\Resources\BookIndexCollection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BookController extends Controller {

    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $filters = $request->only('searchItems');
        $query = isset($filters['searchItems']) ? new BookIndexCollection(
            Book::where('name', 'like', '%' . $filters['searchItems'] . '%')
                ->with('author')
                ->paginate(10)
                ->withQueryString()

        ) : new BookIndexCollection(
            Book::with('author')
                ->paginate(10)
                ->withQueryString()
        );
        return inertia(
            'Book/Index',
            [
                'books' => $query,
                'filters' => $filters
            ]
        );
    }

    /**
     * Display the specified resource.
     */
    public function show(Book $book)
    {

        return inertia('Book/Show', [
            'book' => $book->load('author')
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $authors = Author::all(['id', 'name']);
        return inertia('Book/Create', [
            'authors' => $authors,
            'labels' => __('books.form.labels')
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreBookRequest $request)
    {
        Book::create($request->validated());

        return self::index($request)
            ->with('success', __('books.flash.created'));
    }


    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Book $book)
    {
        $authors = Author::all(['id', 'name']);
        return inertia(
            'Book/Edit',
            [
                'book' => $book,
                'authors' => $authors,
                'labels' => __('books.form.labels')
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateBookRequest $request, Book $book)
    {
        $book->update($request->validated());

        return inertia('Book/Show', [
            'book' => $book,
        ])
            ->with('success', __('books.flash.updated'));
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Book $book)
    {
        $book->delete();

        return redirect()->back()
            ->with('success', __('books.flash.deleted'));
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroyMany(Request $request)
    {
        $ids = $request->input('ids');

        Book::destroy($ids);

        return redirect()->back()
            ->with('success', __('books.flash.delete-many'));
    }
}
