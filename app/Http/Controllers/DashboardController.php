<?php

namespace App\Http\Controllers;

use App\Http\Resources\DashboardIndexCollection;
use App\Models\Book;
use Illuminate\Http\Request;

class DashboardController extends Controller {

    public function index(Request $request)
    {
        $books = Book::latest()->take(5)->get();
        return inertia('Dashboard', [
                'books' => new DashboardIndexCollection($books)
            ]
        );
    }
}
