<?php

namespace App\Http\Requests;

use App\Enum\UserAbilitiesEnum;
use Illuminate\Contracts\Validation\ValidationRule;
use Illuminate\Foundation\Http\FormRequest;

class StoreBookRequest extends FormRequest {
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'name' => ['required', 'string'],
            'author_id' => ['required', 'string'],
            'description' => ['required', 'string'],
            'image' => ['image'],
        ];
    }
}
