<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;

class AuthorIndexCollection extends ResourceCollection {
    /**
     * Transform the resource collection into an array.
     *
     * @return array<int|string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'data' => $this->collection,
            'titles' => [
                'name' => __('authors.titles.name'),
                'biography' => __('authors.titles.biography'),
                'actions_title' => __('authors.titles.actions'),
                'actions' => [
                    'edit' => __('authors.titles.actions.edit'),
                    'delete' => __('authors.titles.actions.delete'),
                ]
            ],
            'actions' => [
                'edit' => 'edit_link',
                'delete' => 'delete_link',
            ]
        ];
    }
}
