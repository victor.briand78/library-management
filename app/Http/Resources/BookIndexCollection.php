<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;

class BookIndexCollection extends ResourceCollection {
    /**
     * Transform the resource collection into an array.
     *
     * @return array<int|string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'data' => $this->collection,
            'titles' => [
                'name' => __('books.titles.name'),
                'author' => __('books.titles.author'),
                'description' => __('books.titles.description'),
                'actions_title' => __('books.titles.actions'),
                'actions' => [
                    'edit' => __('books.titles.actions.edit'),
                    'delete' => __('books.titles.actions.delete'),
                ]
            ],
            'actions' => [
                'edit' => 'edit_link',
                'delete' => 'delete_link',
            ]
        ];
    }
}
