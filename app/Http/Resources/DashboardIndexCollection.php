<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;

class DashboardIndexCollection extends ResourceCollection {
    /**
     * Transform the resource collection into an array.
     *
     * @return array<int|string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'data' => $this->formatCollection($this->collection),
        ];
    }

    static public function formatCollection(object $collection)
    {
        return $collection->map(function ($item) {
            $item->image = asset('storage/books/images/' . $item->image);
            return $item;
        });
    }
}
