<?php

namespace Database\Factories;

use App\Models\Author;
use App\Models\Book;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<Book>
 */
class BookFactory extends Factory {
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'name' => fake()->sentence(3),
            'author_id' => Author::factory(),
            'description' => fake()->sentence(10),
            'image' => fake()->image(storage_path('app/public/books/images'), 400, 300, fullPath: false),
        ];
    }
}
