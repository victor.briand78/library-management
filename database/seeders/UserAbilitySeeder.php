<?php

namespace Database\Seeders;

use App\Enum\UserAbilitiesEnum;
use Illuminate\Database\Seeder;
use Silber\Bouncer\BouncerFacade;

class UserAbilitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $abilities = BouncerFacade::ability()->pluck('name')->toArray();

        foreach (UserAbilitiesEnum::cases() as $ability) {
            if (!in_array($ability, $abilities))
                BouncerFacade::ability()->create([
                    'name' => $ability->value,
                ]);

            if (($key = array_search($ability, $abilities)) !== false) {
                unset($abilities[$key]);
            }
        }

        if (!empty($abilities)) {
            BouncerFacade::ability()->whereIn('name', $abilities)->delete();
        }
    }
}
