<?php

return [
    'titles' => [
        'name' => 'Name',
        'biography' => 'Biography',
        'actions' => 'Actions'
    ],
    'actions' => [
        'title' => 'Actions',
        'edit' => 'Edit',
        'delete' => 'Delete'
    ],
    'form' => [
        'labels' => [
            'name' => 'Name',
            'biography' => 'Biography',
        ]
    ],
    'flash' => [
        'created' => 'The Author has been created',
        'updated' => 'The Author has been updated',
        'deleted' => 'The Author has been deleted',
        'delete-many' => 'Authors deleted',
    ]
];
