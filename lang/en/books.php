<?php

return [
    'titles' => [
        'name' => 'Name',
        'author' => 'Author',
        'description' => 'Description',
        'image' => 'Image',
        'actions' => 'Actions'
    ],
    'actions' => [
        'title' => 'Actions',
        'edit' => 'Edit',
        'delete' => 'Delete'
    ],
    'form' => [
        'labels' => [
            'name' => 'Name',
            'author' => 'Author',
            'description' => 'Description',
            'image' => 'Image'
        ]
    ],
    'flash' => [
        'created' => 'Your book has been created',
        'updated' => 'Your book has been updated',
        'deleted' => 'Your book has been deleted',
        'delete-many' => 'Books deleted',
    ]
];
